<?php
	function validarUsuario($usuario,$password)
	{
		$conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
		$conexion = pg_connect($conn_string);

	    $sql = "select nombres||' '||apellidos nombreapellido from usuarios where usuario = '{$usuario}' and clave = '{$password}';";

	    $datos = pg_query($conexion, $sql);
	    $usuario = pg_fetch_all($datos);

		if ($usuario != null){
			return $usuario;
		} else {
			return false;	
		}		
	}

?>
-------------------------------------------------------------------

<?php

    $id = intval($_POST['idEdit']);
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $marca = intval($_POST['marcaId']);
    $tipo = intval($_POST['tipoId']);

    if($id&&$marca&&$tipo&&$nombre&&$descripcion)
    {

      $conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
	    $conn = pg_connect($conn_string);
      
      $sql = "UPDATE producto set tipo_id=$tipo, marca_id=$marca, nombre='$nombre', descripcion='$descripcion' WHERE producto_id = $id";
      pg_query($conn, $sql);
      echo "Acrualización exitosa";
      header("Refresh: 3;url=mantener-producto.php");

    }else{
      echo "Debe enviar los datos obligatorios para la edición (Marca, Tipo, Nombre y Descripción)";
      header("Refresh: 3;url=obtener-datos.php");
    }
?>
---------------------------------------------------------------------------------------------------------------

<?php
    $id = $_POST['idDelete'];

    if($id)
    {
      $conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
      $conexion = pg_connect($conn_string);
      
      $sql = "delete from producto where producto_id = '{$id}'";
      $result = pg_query($conexion, $sql);

      if ($result != null){
        echo "El registro se ha eliminado correctamente";
        header("Refresh: 3;url=mantener-producto.php");
      } else {
        echo "Error al eliminar el registro";
        header("Refresh: 3;url=mantener-producto.php");
      }

    }else{
      echo "Debe enviar el ID del registro a eliminar";
      header("Refresh: 3;url=mantener-producto.php");
    }
?>
----------------------------------------------------------------------------------------------------
<html lang = "en">
   
   <head>
      <title>Ejercitario 5</title>
   </head>

   <body>
      <h3>Ingrese su Usuario y Contraseña</h3>
      <div class = "container">
         <form role="form" method="POST" action="login.php">
            <input type = "text" name = "usuario" placeholder = "usuario" required autofocus>
            <br><br>
            <input type = "password" name = "password" placeholder = "contraseña" required>
            <br><br>
            <button type = "submit" name = "login">Login</button>
         </form>
      </div> 
   </body>
</html>
-----------------------------------------------------------------------------------------------------

<?php
    $nombreProducto = $_POST['nombreProducto'];
    $descProducto = $_POST['descProducto'];
    $marcaProducto = intval($_POST['marcaProducto']);
    $tipoProducto = intval($_POST['tipoProducto']);

    if($nombreProducto&&$descProducto&&$marcaProducto&&$tipoProducto)
    {
      $conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
      $conexion = pg_connect($conn_string);
      
      $sql = "INSERT INTO producto (tipo_id,marca_id,nombre,descripcion) VALUES ($tipoProducto,$marcaProducto,'$nombreProducto','$descProducto')";
      $result = pg_query($conexion, $sql);
      
      if ($result != null){
        echo "Datos ingresados correctamente";
        header("Refresh: 3;url=mantener-producto.php");
      } else {
        echo "Error al ingresar los datos";
        header("Refresh: 3;url=mantener-producto.php");
      }

    }else{
      echo "Debe ingresar los datos obligatorios del producto: Nombre, Descripción, Marca y Tipo";
      header("Refresh: 3;url=mantener-producto.php");
    }
?>
---------------------------------------------------------------------------------------------------

<?php
    require_once 'control.php';

    $usuario = $_POST['usuario'];
    $password = $_POST['password'];  
    
    if($usuario&&$password)
    {
      
      $usu = validarUsuario($usuario,$password);

      if($usu != null)
      {
        foreach($usu as $array)
        {
          echo '<span> Usuario en sesión: '. $array['nombreapellido'].'</span><hr>';
        }
        include 'mantener-producto.php';
      } 
      else
      {
        echo "El usuario o contraseña introducido no es válido";
        header("Refresh: 3;url=formulario.php");
      }
    }
    else
    {  
      echo "Error al ingresar el usuario o contraseña";
    }
?>
-------------------------------------------------------------------------------

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 5</title>
  </head>
  <body>
    <?php
    	$conn = new PDO('pgsql:host=localhost;dbname=ejercitario5;', 'postgres', 'postgres');
		$sqlProductos = 'select p.producto_id producto_id, p.nombre producto_nombre, p.descripcion producto_descripcion, m.nombre producto_marca, t.nombre producto_tipo
					from producto p
					join marca m on p.marca_id = m.marca_id 
					join tipo t on p.tipo_id = t.tipo_id
					order by 2;';
		$sqlMarca = 'select * from marca';
		$sqlTipo = 'select * from tipo';

		echo'
			<div class = "container">
				<form role="form" method="POST" action="guardar-datos.php">
					<fieldset>
					<legend>Datos del producto a ser ingresado ó modificado</legend>
						<input type="hidden" name="codigo">
						<input type="text" name="nombreProducto" placeholder="Nombre del Producto" required autofocus>
						<br><br>
						<input type = "text" name = "descProducto" placeholder = "Descripción" required autofocus>
						<br>
						<p>Seleccione una marca</p>
						<select name="marcaProducto" id="idMarca">';
						foreach ($conn->query($sqlMarca) as $array1)
							{ 
								echo '<option value="'. $array1['marca_id'].'">'. $array1['nombre'].'</option>';
							}
						echo '</select>
						<br>
						<p>Seleccione un tipo</p>
						<select name="tipoProducto" id="idTipo">';
						foreach ($conn->query($sqlTipo) as $array2)
							{
								echo '<option value="'. $array2['tipo_id'].'">'. $array2['nombre'].'</option>';
							}
						echo '</select>
						<br><br>
						<button type = "submit" name = "guardarDatos">Guardar</button>
						<br>
					</fieldset>
	            </form>
            </div> 
			
			<h3>Productos actuales</h3>
			
			<table>
				<tr>
				<th>Codigo</th>
				<th>Producto</th>
				<th>Descripción</th>
				<th>Marca</th>
				<th>Tipo</th>
				<th>Operación</th>
				<th>Operación</th>
				</tr>
				';
				foreach ($conn->query($sqlProductos) as $array3) {
					echo'<tr>
							<td>'. $array3['producto_id'].'</td>
							<td>'. $array3['producto_nombre'].'</td>
							<td>'. $array3['producto_descripcion'].'</td>
							<td>'. $array3['producto_marca'].'</td>
							<td>'. $array3['producto_tipo'].'</td>
							<td>  
							<form method="post" action="eliminar-datos.php"> 
								<input type="submit" name="delete" value="Borrar 🗑️" >
								<input type="hidden" name="idDelete" value="'.$array3['producto_id'].'">
							</form>
							</td>
							<td><a class="btn btn-warning" href=obtener-datos.php?idEdit="'.$array3['producto_id'].'">Editar 📝</a></td>
						</tr>';
				}
		    echo'</table>
				<style>
				table,th, td 
				{
				border: 1px solid black;
				border-collapse: collapse;
				padding: 5px;
				}			
				</style>
				';
    ?>
  </body>
</html>
---------------------------------------------------------------------------------------------------

<?php
    
    $conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
	$conn = pg_connect($conn_string);
    
    if(count($_POST)>0) {
      pg_query($conn,"UPDATE producto set tipo_id='" . $_POST['tipoId'] . "', marca_id='" . $_POST['marcaId'] . "', nombre='" . $_POST['nombre'] . "', descripcion='" . $_POST['descripcion'] . "' WHERE producto_id='" . $_POST['idEdit'] . "'");
      $message = "Record Modified Successfully";
	}
	
    $id = $_GET['idEdit'];
	$idInt = trim($id, "'\"");

    $sql = "select * from producto where producto_id = '{$idInt}'";

	$datos = pg_query($conn, $sql);
	$row = pg_fetch_array($datos);
	
	$sqlMarca = 'select * from marca';
	$sqlTipo = 'select * from tipo';

	$marcas_query = pg_query($conn, $sqlMarca);
	$tipos_query = pg_query($conn, $sqlTipo);

	$marcas=pg_fetch_all($marcas_query);
	$tipos=pg_fetch_all($tipos_query);

?>
	<html>
      <head>
        <title>Update Data</title>
      </head>

      <body>
        <form name="frm" method="post" action="editar-datos.php">
        <div><?php if(isset($message)) { echo $message; } ?>
        </div>
        <div style="padding-bottom:5px;">
        <a href="mantener-producto.php">Volver al listado de productos</a>
        </div>
        <input type="hidden" name="idEdit" class="txtField" value="<?php echo $row['producto_id']; ?>">
		<p>Seleccione una marca</p>
		<select name="marcaId" id="idMarca">';
		<?php foreach ($marcas as $array1)
			{ 
				echo '<option value="'.$array1['marca_id'].'">'.$array1['nombre'].'</option>';
			}
		?>
		echo '</select>
		<br>
		<p>Seleccione un tipo</p>
		<select name="tipoId" id="idTipo">';
		<?php foreach ($tipos as $array2)
			{
				echo '<option value="'.$array2['tipo_id'].'">'.$array2['nombre'].'</option>';
			}
		?>
		</select>
        <br><br>
        Nombre:<br>
        <input type="text" name="nombre" class="txtField" value="<?php echo $row['nombre']; ?>">
        <br><br>
        Descripción:<br>
        <input type="text" name="descripcion" class="txtField" value="<?php echo $row['descripcion']; ?>">

		
	
        <br><br>
        <input type="submit" name="submit" value="Guardar cambios" class="buttom">

        </form>
      </body>
	</html>
